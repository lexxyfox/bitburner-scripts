export const doc = eval('document')

let _player, _router, _terminal, _suffix

_: for (const elem of doc.body.getElementsByClassName('MuiBox-root'))
  for (const [key, val] of Object.entries(elem))
    if (key.startsWith('__reactProps')) 
      try {
        _player   ??= val.children.props.player
        _router   ??= val.children.props.router
        _terminal ??= val.children.props.terminal
        _suffix   ??= key.substring(13)
        if (_player && _router && _terminal)
          break _
      } catch {}

export const player = _player
export const router = _router
export const terminal = _terminal
export const suffix = _suffix
