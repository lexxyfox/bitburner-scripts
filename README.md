My random assortment of unassorted scripts for a lovely little game called βїէВ∐𐅾η⋿®

## overview-extra.js

```https://crux.land/6sXPV9```

The simplest, ["intended"](https://github.com/danielyxie/bitburner/blob/master/src/ui/React/CharacterOverview.tsx#L489-L503) method of adding content to the overview box (at no RAM cost). Overview Extra consists of three columns, each of them an [HTMLElement](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement) that the user is free to do whatever HTML modifications on they desire.

* `left` The left column - similar to HP, Money, Hack, Str, etc. Text is left aligned.
* `right` The "right" column - aligned with the stat numbers. Text is right aligned.
* `after` A third column to the right of "right". Not typically used. Text is left aligned.

![overview columns](https://gitlab.com/lexxyfox/bitburner-scripts/-/wikis/overview-extra.png)

## exploits_without_document.js

Run without any arguments. Unlocks the following SF-1 exploits:

* Bypass
* Prototype Tampering
* Rainbow
* Time compression
* Unclickable
* Undocumented Function

And does so without abusing eval(), unnecessarily accessing the document global, mucking around with CSS styles, or using unnecessary variables or functions. Also does so with only base RAM usage and can be run repeatedly without errors. 

## internal.js

```https://crux.land/2Mgzkq```

This javascript module is for super cheaters. Allows access to several internal game variables from Javascript (at no RAM cost). Exports are as follows

* `doc` The page's [HTMLDocument](https://developer.mozilla.org/en-US/docs/Web/API/Document), at no RAM cost.
* `player` The global [Player](https://github.com/danielyxie/bitburner/blob/dev/src/PersonObjects/Player/PlayerObject.ts#L30-L171) object (you)
* `router` The global [Router](https://github.com/danielyxie/bitburner/blob/dev/src/ui/GameRoot.tsx#L195-L272) - lets you switch to different pages such as Terminal, City, Augmentations, DevMenu, etc. Of note, destroying a BitNode is a page (`BitVerse`).
* `terminal` The global [Terminal](https://github.com/danielyxie/bitburner/blob/dev/src/Terminal/Terminal.ts#L78-L848)

### Examples

```js
import * as internal from 'https://crux.land/2Mgzkq' 

/** @param {NS} _ */
export let main = _ => {
  // clears the terminal
  internal.terminal.clear()

  // print current money
  _.tprint(internal.player.money)

  // give $1b
  internal.player.money += 1E9
  
  // open dev menu
  internal.router.toDevMenu()
}
```

